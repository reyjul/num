#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import click
import ldap
import ldap.modlist
import os
import sys
import unicodedata
import re
import random

from envelopes import Envelope
from fabric import Connection
from grampg import PasswordGenerator
from paramiko.ssh_exception import AuthenticationException
from passlib.hash import sha512_crypt
from jinja2 import Environment, PackageLoader
from yaml import load, dump
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

if os.path.exists(os.path.join(os.path.dirname(__file__), 'config', 'conf.yml')):
    config_path = os.path.join(os.path.dirname(__file__), 'config', 'conf.yml')
elif os.path.exists('conf.yml'):
    config_path = 'conf.yml'
elif os.path.exists('/etc/num/conf.yml'):
    config_path = '/etc/num/conf.yml'
else:
    click.echo("Error: No config file found.")
    sys.exit(1)

jinja_env = Environment(loader=PackageLoader(__name__, "templates"), keep_trailing_newline=True)

default_parameters = {
    'ldap_server': 'localhost',
    'ldap_password': '',
    'ldap_use_tls': False,
    'ldap_user_dn_prefix': 'uid',
    'ldap_base_dn': 'dc=ifb,dc=local',
    'ldap_bind_dn': 'uid=admin,ou=people,dc=ifb,dc=local',
    'ldap_user_dn_prefix': 'uid',
    'ldap_services_ou': 'ou=Services,ou=People,dc=ifb,dc=local',
    'ldap_people_ou': 'ou=People,dc=ifb,dc=local',
    'ldap_groups_ou': 'ou=groups,dc=ifb,dc=local',
    'ldap_projects_groups_ou': 'ou=projects,ou=groups,dc=ifb,dc=local',
    'ldap_applications_groups_ou': 'ou=applications,ou=groups,dc=ifb,dc=local',
    'ldap_user_groups_ou': 'ou=user-groups,ou=groups,dc=ifb,dc=local',
    'len_firstname': 1,
    'len_lastname': 0,
    'min_uid_number': 1000,
    'min_gid_number': 10000,
    'min_project_gid_number': 10000,
    'min_service_gid_number': 10000,
    'default_inode_factor': 500,
    'default_hard_quota_factor': 1.5,
    'client_server': 'localhost',
    'client_user': 'root',
    'user_default_soft_quota': '100G',
    'user_home': '/shared/home',
    'quota_home': False,
    'project_default_soft_quota': '1000G',
    'project_home': '/shared/projects',
    'project_mkdir_template': 'mkdir -p {PATH}',
    'project_quota_template': 'lfs setquota -p {GIDNUMBER} --block-softlimit {SOFTQUOTA} --block-hardlimit {HARDQUOTA} --inode-softlimit {INODESOFTLIMIT} --inode-hardlimit {INODEHARDLIMIT} {PATH}',
    'setgid': False,
    'setfacl_right_uppercase': False,
    'setfacl_project_template': 'setfacl {OPTIONS} -m group:{GROUP}:{RIGHTS} {PATH}; setfacl -m default:group:{GROUP}:{RIGHTS} {PATH}',
    'setfacl_project_remove_template': 'setfacl {OPTIONS} -x group:{GROUP} {PATH}; setfacl -x default:group:{GROUP} {PATH}',
    'scratch_default_soft_quota': '1000G',
    'scratch_home': '/shared/scratch',
    'scratch_mkdir_template': 'mkdir -p {PATH}',
    'scratch_quota_template': 'lfs setquota -p {GIDNUMBER} --block-softlimit {SOFTQUOTA} --block-hardlimit {HARDQUOTA} --inode-softlimit {INODESOFTLIMIT} --inode-hardlimit {INODEHARDLIMIT} {PATH}',
    'default_account': 'demo',
    'open_partitions': ['fast'],
    'notification_sender': 'root',
    'notification_sender_name': 'num',
    'create_user_subject': 'Your account.',
    'add_user_to_project_subject': 'Your project.',
    'smtp_host': 'localhost',
}


config = load(open(config_path, 'r', encoding='utf-8'), Loader=Loader)
for param in default_parameters:
    config.setdefault(param, default_parameters[param])


@click.group()
@click.version_option(version="1.0")
def num():
    # check if ldap is reachable
    try:
        ldap_connection = _ldap_connection()
    except ldap.LDAPError as e:
        click.echo("Error: Unable to access ldap server.")
        click.echo(str(e))
        sys.exit(1)

    ldap_connection.unbind_s()

    # check if cluster login is reachable
    try:
        run_command(config['client_server'], config['client_user'], "ls", hide=True)
    except AuthenticationException:
        click.echo("Error: Unable to acces cluster login node.")
        click.echo("You may need to run the following command : ssh-copy-id %s@%s" % (config['client_user'], config['client_server']))
        sys.exit(1)


@num.command()
@click.option('--uid', default=None, help='filter by User Id')
@click.option('--uidn', default=None, help='filter by User Id Number')
@click.option('--gidn', default=None, help='filter by Group Id Number')
@click.option('--cn', default=None, help='filter by Common Name')
@click.option('--ou', default=None, help='filter by Organizational Unit')
@click.option('--objc', default=None, help='filter by objectClass')
@click.option('--filt', default=None, help='use a custom filter')
@click.option('--full/--compact', ' /-c', default=True, help='compact output')
def show_ldap(uid, uidn, gidn, cn, ou, objc, filt, full):
    """ show ldap info """

    ldap_filter = None
    if uid:
        ldap_filter = '(uid=' + uid + ')'
    if uidn:
        ldap_filter = '(uidNumber=' + uidn + ')'
    if gidn:
        ldap_filter = '(gidNumber=' + gidn + ')'
    if cn:
        ldap_filter = '(cn=' + cn + ')'
    if ou:
        ldap_filter = '(ou:dn:=' + ou + ')'
    if objc:
        ldap_filter = '(objectClass=' + objc + ')'
    if filt:
        ldap_filter = filt

    if ldap_filter:
        click.echo("Info: Search for %s." % ldap_filter)
        try:
            ldap_connection = _ldap_connection()
            results = ldap_connection.search_s(base=config['ldap_base_dn'],
                                               scope=ldap.SCOPE_SUBTREE,
                                               filterstr=ldap_filter)
            ldap_connection.unbind_s()
            click.echo("#" * 80)
            for res in results:
                dn, entry = res
                entry = {k: [v2.decode() for v2 in v] for k, v in entry.items()}
                click.echo("dn: " + dn)
                if full:
                    click.echo("-" * 80)
                    click.echo(dump(entry))
                else:
                    click.echo(entry)
                click.echo("#" * 80)
        except ldap.LDAPError as e:
            click.echo(str(e))
            sys.exit(1)


@num.command()
@click.argument('name')
@click.option('-p', '--parent', default=None)
@click.option('--slurm/--no-slurm', default=True)
@click.option('-sq', '--softquota', default=config['project_default_soft_quota'])
def create_project(name, parent, slurm, softquota):
    """ Create a project on the NNCR Cluster infrastructure """

    # cleanup project name (all lower case, no space)
    project = name.replace(' ', '').lower()

    if parent:
        # check if parent ldap group exists
        if not project_group_exists(parent):
            click.echo("Error: Parent project group %s does not exist." % parent)
            sys.exit(1)

        # check if projet OU already exists
        if not project_ou_exists(parent):
            dn = 'ou=%s,%s' % (parent, config['ldap_projects_groups_ou'])
            _ldap_add_ou_recursively(dn)

        ou = "ou=%s,%s" % (parent, config['ldap_projects_groups_ou'])
        project_group = "%s_%s" % (parent, project)
    else:
        ou = config['ldap_projects_groups_ou']
        project_group = project

    # check if another group exist with same name
    if group_exists(project_group):
        click.echo("Error: Project group %s already exists." % project_group)
        sys.exit(1)

    # find first available gidNumber
    gid_number = get_next_gid_number(config['min_project_gid_number'])
    if not gid_number:
        click.echo("Error: No more gid number available.")
        sys.exit(1)

    # create ldap group
    dn = 'cn=%s,%s' % (project_group, ou)
    _ldap_add_group(dn, gid_number)

    # create project folder
    # mdt-index with value "-1" means that the folder will be created
    # on the mdt with with the most available inodes, in percentage.
    # it's an easy autobalance solution.
    project_path = get_project_path(name, parent, "project")
    click.echo("Info: Creating project directory %s." % project_path)
    cmd_mkdir_template = config['project_mkdir_template'].format(PATH=project_path)
    run_command(config['client_server'], config['client_user'], cmd_mkdir_template)
    run_command(config['client_server'], config['client_user'], 'chmod 770 %s' % project_path)
    if config['setgid']:
        run_command(config['client_server'], config['client_user'], 'chmod g+s %s' % project_path)

    # set acl on project folder
    _set_acl(project_path, project_group, rights='rwx')

    # set quota on user project folder if there is not parent, else the quota is fixed on the parent folder
    if not parent:
        _set_quota('project', project_path, gid_number, softquota)

    # remove parent project default and group acl
    if parent:
        _set_acl(project_path, parent, remove=True)

    # create slurm account for project
    if slurm:
        click.echo("Info: Creating Slurm account %s." % project_group)
        account_cmd = 'sacctmgr -i add account %s defaultqos=normal qos=normal' % project_group
        if parent:
            account_cmd += ' parent=%s' % parent
        run_command(config['client_server'], config['client_user'], account_cmd, hide=True, warn=True)

    _run_extra_cmd('project', project_path)


@num.command()
@click.argument('name')
@click.option('-p', '--parent', default=None)
@click.option('-sq', '--softquota', default=config['scratch_default_soft_quota'])
def create_scratch(name, parent, softquota):
    """ Create a scratch on the NNCR Cluster infrastructure """

    # cleanup project name (all lower case, no space)
    project = name.replace(' ', '').lower()

    if parent:
        project_group = "%s_%s" % (parent, project)
    else:
        project_group = project

    if not project_group_exists(name, parent):
        click.echo("Error: Project %s does not exist." % project)
        sys.exit(1)

    # create project folder
    project_path = get_project_path(name, parent, "scratch")
    click.echo("Info: Creating scratch directory %s." % project_path)
    cmd_mkdir_template = config['scratch_mkdir_template'].format(PATH=project_path)
    run_command(config['client_server'], config['client_user'], cmd_mkdir_template)
    run_command(config['client_server'], config['client_user'], 'chmod 770 %s' % project_path)
    if config['setgid']:
        run_command(config['client_server'], config['client_user'], 'chmod g+s %s' % project_path)

    # set acl on project folder
    _set_acl(project_path, project_group, rights='rwx')

    # set quota on user project folder if there is not parent, else the quota is fixed on the parent folder
    gid_number = _get_group_gid(project)
    if not parent:
        _set_quota('scratch', project_path, gid_number, softquota)

    # remove parent project default and group acl
    if parent:
        _set_acl(project_path, parent, remove=True)

    _run_extra_cmd('scratch', project_path)


@num.command()
@click.argument('name')
@click.option('-p', '--parent', default=None)
@click.option('--data/--no-data', default=False)
@click.option('--slurm/--no-slurm', default=True)
def remove_project(name, parent, data, slurm):
    """ remove a project """

    if parent:
        # check if parent OU, parent group and subproject group exist
        project_group = "%s_%s" % (parent, name)
        if project_ou_exists(parent) and project_group_exists(parent) and group_exists(project_group):
            remove_project_group(name, parent)
        else:
            click.echo("Info: Parent project %s does not exist. Skipping LDAP management." % parent)
    else:
        project_group = name

        # remove OU
        if project_ou_exists(name):

            subprojects = retrieve_subprojects(name)
            for subproject in subprojects:
                remove_project_group(subproject, name)
                if slurm:
                    remove_slurm_account("%s_%s" % (name, subproject))

            remove_project_ou(name)

        # check if project exists
        if project_group_exists(project_group):
            remove_project_group(project_group)
        else:
            click.echo("Info: Project group %s does not exist. Skipping LDAP management." % project_group)

    if slurm:
        remove_slurm_account(project_group)
    if data:
        remove_project_folder(name, parent)


@num.command()
@click.argument('name')
def create_group(name):
    """ Create a group on the NNCR Cluster infrastructure """

    # cleanup project name (all lower case, no space)
    groupname = name.replace(' ', '').lower()
    ou = config['ldap_groups_ou']

    # check if another group exist with same name
    if group_exists(name):
        click.echo("Error: Group name %s already exists." % name)
        sys.exit(1)

    # find first available gidNumber
    gid_number = get_next_gid_number(config['min_gid_number'])
    if not gid_number:
        click.echo("Error: No more gid number available.")
        sys.exit(1)

    # create ldap group
    dn = 'cn=%s,%s' % (groupname, ou)
    _ldap_add_group(dn, gid_number)


@num.command()
@click.argument('name')
def remove_group(name):
    """ remove a group """

    # cleanup project name (all lower case, no space)
    groupname = name.replace(' ', '').lower()
    ou = config['ldap_groups_ou']

    _ldap_remove_group(groupname, ou)


@num.command()
@click.option('--firstname', prompt='First name of the user', help='First name of the new user.')
@click.option('--lastname', prompt='Last name of the user', help='Last name of the new user.')
@click.option('--email', help='Mail address of the user.')
@click.option('--service', is_flag=True, help='This account is a service account.')
@click.option('--uid_number', default=None)
@click.option('--gid_number', default=None)
@click.option('--home_dir', default=None)
@click.option('--username', default=None)
@click.option('--groupname', default=None)
@click.option('--password', default=None)
@click.option('--notify', is_flag=True)
@click.option('--softquota', default=config['user_default_soft_quota'])
@click.option('--slurm/--no-slurm', default=True)
def create_user(firstname, lastname, email, service, uid_number, gid_number, home_dir, username, groupname, password, notify, softquota, slurm):
    """ create a user in ldap directory """

    if service:
        dn_suffix = config['ldap_services_ou']
        _ldap_add_ou_recursively(config['ldap_services_ou'])
    else:
        dn_suffix = config['ldap_people_ou']
        _ldap_add_ou_recursively(config['ldap_people_ou'])
        if not email:
            click.echo("Error: Email is mandatory to create user account.")
            sys.exit(1)
        if email_exists(email):
            click.echo("Error: Email %s already exists." % email)
            sys.exit(1)

    dn_suffix_groups = config['ldap_user_groups_ou']
    _ldap_add_ou_recursively(config['ldap_user_groups_ou'])

    if username is None:
        username = generate_username(firstname, lastname, config['len_firstname'], config['len_lastname'])

    # it may be because it have been provided, or because generate_username loop have failed
    if username_exists(username):
        click.echo("Error: Username %s already exists." % username)
        sys.exit(1)

    if groupname:
        if group_exists(groupname):
            click.echo("Info: Group name %s already exists." % (groupname))
            gid_number = _get_group_gid(groupname)
        else:
            gid_number = get_next_gid_number(config['min_gid_number'])
            dn_group = 'cn=' + groupname + ',' + dn_suffix_groups
            _ldap_add_group(dn_group, gid_number)
    elif gid_number is None:
        if uid_number is None:
            # if both uid_number and gid_number are None, we find the next common uid/gid number
            uid_number = get_next_uid_gid_number(config['min_uid_number'], config['min_gid_number'])
        gid_number = uid_number
        dn_group = 'cn=' + username + ',' + dn_suffix_groups
        _ldap_add_group(dn_group, gid_number)

    if uid_number is None:
        uid_number = get_next_uid_number(config['min_uid_number'])

    if home_dir is None:
        home_dir = os.path.join(config['user_home'], username)

    # generate password
    if password is None:
        password = (PasswordGenerator().of().between(3, 6, 'letters')
                    .at_least(4, 'numbers')
                    .length(10)
                    .beginning_with('letters')
                    .done()).generate()

    # create user ldap account
    dn_prefix = config['ldap_user_dn_prefix']
    if dn_prefix == "uid":
        dn_user = dn_prefix + '=' + username + ',' + dn_suffix
    elif dn_prefix == "cn":
        dn_user = dn_prefix + '=' + firstname + ' ' + lastname + ',' + dn_suffix
    modlist = {
        "objectClass": ["inetOrgPerson".encode(), "posixAccount".encode(), "shadowAccount".encode()],
        "uid": [(str(username)).encode()],
        "sn": [(str(lastname)).encode()],
        "givenName": [(str(firstname)).encode()],
        "cn": [(str(firstname + " " + lastname)).encode()],
        "displayName": [(str(firstname + " " + lastname)).encode()],
        "uidNumber": [(str(uid_number)).encode()],
        "gidNumber": [(str(gid_number)).encode()],
        "loginShell": ["/bin/bash".encode()],
        "homeDirectory": [(str(home_dir)).encode()],
        "userPassword": [(str(generate_password_hash(password))).encode()]
    }
    # add email only if provided
    if email:
        modlist['mail'] = [(str(email)).encode()]

    try:
        ldap_connection = _ldap_connection()
        ldap_connection.add_s(dn_user, ldap.modlist.addModlist(modlist))
        ldap_connection.unbind_s()
        click.echo("Info: Added user %s (uid=%s, gid=%s) with password %s." % (username, uid_number, gid_number, password))
    except ldap.LDAPError as e:
        click.echo("Error: Unable to create user entry.")
        click.echo(str(e))
        sys.exit(1)

    # create homedir
    click.echo("Info: Creating home directory %s." % home_dir)
    run_command(config['client_server'], config['client_user'], '/sbin/mkhomedir_helper {} 0077'.format(username))
    if config.get('quota_home', False):
        _set_quota('home', home_dir, gid_number, softquota)

    if slurm:
        _slurm_set_default_account_as_demo(username)

    # envoi d'un mail à l'utilisateur
    if notify and email:
        send_email_create_user(email, firstname, username, password)

    _run_extra_cmd('home', home_dir)


@num.command()
@click.argument('username')
@click.option('--data/--no-data', default=True)
@click.option('--slurm/--no-slurm', default=True)
def remove_user(username, data, slurm):
    """ remove a user in ldap directory """
    if not username_exists(username):
        click.echo("Error: Username %s does not exist." % username)
        sys.exit(1)

    # delete user entry in ldap
    try:
        ldap_connection = _ldap_connection()
        results = ldap_connection.search_s(base=config['ldap_base_dn'],
                                           scope=ldap.SCOPE_SUBTREE,
                                           filterstr='uid=' + username,
                                           attrlist=['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)
            click.echo("Info: Removed user dn %s." % dn)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Error: Unable to delete user %s." % username)
        click.echo(str(e))
        sys.exit(1)

    # delete user group entry in ldap
    try:
        ldap_connection = _ldap_connection()
        results = ldap_connection.search_s(base=config['ldap_user_groups_ou'],
                                           scope=ldap.SCOPE_SUBTREE,
                                           filterstr='cn=' + username,
                                           attrlist=['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)
            click.echo("Info: Removed group dn %s." % dn)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Info: Unable to delete user group %s." % username)
        click.echo(str(e))

    # TODO: use _ldap_modify_any_user_group
    # todo: maybe do it before remove user entry
    # remove user from groups in ldap
    try:
        ldap_connection = _ldap_connection()
        results = ldap_connection.search_s(base=config['ldap_groups_ou'],
                                           scope=ldap.SCOPE_SUBTREE,
                                           filterstr='memberUid=' + username,
                                           attrlist=['dn', 'memberUid'])
        for dn, entry in results:
            mod_attrs = [(ldap.MOD_DELETE, 'memberUid', [username.encode()])]
            ldap_connection.modify_s(dn, mod_attrs)

        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Error: Unable to remove user %s from groups." % username)
        click.echo(str(e))
        sys.exit(1)

    if slurm:
        remove_slurm_user(username)
    if data:
        remove_user_folder(username)


def remove_slurm_user(name):
    click.echo("Info: Removing Slurm user %s." % name)
    account_cmd = 'sacctmgr -Q -i delete user %s' % name
    result = run_command(config['client_server'], config['client_user'], account_cmd, hide=True, warn=True)
    if not result.ok and "Nothing deleted" not in result.stdout:
        click.echo("Info: Failed to delete Slurm user %s." % name)


def remove_user_folder(username):
    home_dir = os.path.join(config['user_home'], username)
    if home_dir and len(home_dir) > len(config['user_home']):
        click.echo("Info: Removing user home directory %s." % home_dir)
        cmd = 'rm -rf %s' % home_dir
        run_command(config['client_server'], config['client_user'], cmd)


@num.command()
@click.option('-p', '--parent', default=None)
@click.argument('username')
@click.argument('project')
@click.argument('partition')
@click.option('-m', '--mins', default=None)
def add_user_to_partition(parent, username, project, partition, mins):
    """ Add an existing user to an existing partition """

    if not username_exists(username):
        click.echo("Error: Username %s does not exist." % username)
        sys.exit(1)

    if not project_group_exists(project, parent):
        click.echo("Error: Project group %s does not exist." % project)
        sys.exit(1)

    (project_group, ou) = get_project_group_and_ou(project, parent)

    _slurm_modify_user_partition(username, project_group, partition, "add", mins)


@num.command()
@click.option('-p', '--parent', default=None)
@click.argument('username')
@click.argument('project')
@click.argument('partition')
def remove_user_from_partition(parent, username, project, partition):
    """ Add an existing user to an existing partition """

    if not username_exists(username):
        click.echo("Error: Username %s does not exist." % username)
        sys.exit(1)

    if not project_group_exists(project, parent):
        click.echo("Error: Project group %s does not exist." % project)
        sys.exit(1)

    (project_group, ou) = get_project_group_and_ou(project, parent)

    _slurm_modify_user_partition(username, project_group, partition, "remove")


def _slurm_modify_user_partition(username, account, partition, mode="add", mins=None):

    # todo check if account and username exist
    if len(partition) > 0:
        click.echo("Info: Slurm " + mode + " association user " + username + " / account " + account + " / partition " + partition)

        cmd_part = 'sacctmgr -i {mode} user {username} account={account} partitions={partition}'.format(mode=mode,
                                                                                                        username=username,
                                                                                                        account=account,
                                                                                                        partition=partition)

        if mins and mode == "add":
            cmd_part += ' GrpCPUMins=' + str(mins)

        run_command(config['client_server'], config['client_user'], cmd_part, hide=True, warn=True)


@num.command()
@click.option('-p', '--parent', default=None)
@click.option('--slurm/--no-slurm', default=True)
@click.option('--notify', is_flag=True)
@click.argument('username')
@click.argument('project')
def add_user_to_project(parent, slurm, notify, username, project):
    """ Add an existing user to an existing project """

    if not username_exists(username):
        click.echo("Error: Username %s does not exist." % username)
        sys.exit(1)

    if not project_group_exists(project, parent):
        click.echo("Error: Project group %s does not exist." % project)
        sys.exit(1)

    _ldap_modify_user_and_project(parent, username, project, ldap.MOD_ADD)

    (project_group, ou) = get_project_group_and_ou(project, parent)
    if slurm:
        # create slurm user for default project
        for partition in config['open_partitions']:
            _slurm_modify_user_partition(username, project_group, partition)

        # disabled as we now set user default account to demo
        # _slurm_set_default_account(username, project_group)

    # envoi d'un mail à l'utilisateur
    if notify:
        send_email_add_user_to_project(username, project_group, project, parent, slurm)


def _slurm_clean_default_account(account):
    if _slurm_check_if_account_exist(account):
        # todo should check if account is not demo
        users_with_defaccount_cmd = 'sacctmgr -n -P list user defaultaccount={} format=user'.format(account)
        users = run_command(config['client_server'], config['client_user'], users_with_defaccount_cmd, hide=True, warn=True)
        for user in users.stdout.splitlines():
            _slurm_set_default_account_as_demo(user)


def _slurm_set_default_account_as_demo(username):
    demo_account = config['default_account']
    demo_partition = config['open_partitions'][0]  # should be fast, todo : add this in config too
    demo_mins = 6 * 60  # 6h
    # check if demo account exist:

    if not _slurm_check_if_account_exist(demo_account):
        # todo find if we should change qos
        create_cmd = "sacctmgr -i add account {account} defaultqos=normal qos=normal".format(account=demo_account)
        run_command(config['client_server'], config['client_user'], create_cmd, hide=True, warn=True)

    _slurm_modify_user_partition(username, demo_account, demo_partition, "add", demo_mins)
    _slurm_set_default_account(username, demo_account)


def _slurm_check_if_account_exist(account):
    chk_cmd = "sacctmgr -n -P list account account={account} format=account".format(account=account)

    cmd_res = run_command(config['client_server'], config['client_user'], chk_cmd, hide=True, warn=True)
    accounts = cmd_res.stdout.splitlines()

    return len(accounts) > 0


@num.command()
@click.argument('username')
@click.argument('account')
def set_user_default_account(username, account):
    """ Set default account to an existing user """
    if _slurm_check_if_account_exist(account) and username_exists(username):
        _slurm_set_default_account(username, account)


def _slurm_set_default_account(username, account):

    click.echo("Info: Slurm set default account %s for user %s." % (account, username))
    set_defaccount_cmd = 'sacctmgr -i modify user where user={username} set defaultaccount={account}'.format(username=username, account=account)
    run_command(config['client_server'], config['client_user'], set_defaccount_cmd, hide=True, warn=True)


def _is_user_default_account(username, account):
    users_with_defaccount_cmd = 'sacctmgr -n -P list user defaultaccount={account} user={username} format=user'.format(username=username,
                                                                                                                       account=account)
    users = run_command(config['client_server'], config['client_user'], users_with_defaccount_cmd, hide=True, warn=True)
    return len(users.stdout.splitlines()) > 0


@num.command()
@click.option('-p', '--parent', default=None)
@click.option('--slurm/--no-slurm', default=True)
@click.argument('username')
@click.argument('project')
def remove_user_from_project(parent, slurm, username, project):
    """ Remove an existing user from an existing project """
    (account, ou) = get_project_group_and_ou(project, parent)
    _ldap_modify_user_and_project(parent, username, project, ldap.MOD_DELETE)
    if slurm:
        if _is_user_default_account(username, account):
            _slurm_set_default_account_as_demo(username)

        if _slurm_check_if_account_exist(account):
            cmd_account_partition = 'sacctmgr -nP list associations account={account} format=partition'.format(account=account)
            parts = run_command(config['client_server'], config['client_user'], cmd_account_partition, hide=True, warn=True)
            for partition in list(set(parts.stdout.splitlines())):
                _slurm_modify_user_partition(username, account, partition, 'remove')


@num.command()
@click.argument('username')
@click.argument('app')
def add_user_to_app(username, app):
    """ Add an existing user to an existing application """
    _ldap_modify_user_and_application(username, app, ldap.MOD_ADD)


@num.command()
@click.argument('username')
@click.argument('app')
def remove_user_from_app(username, app):
    """ Remove an existing user from an existing application """
    _ldap_modify_user_and_application(username, app, ldap.MOD_DELETE)


@num.command()
@click.argument('username')
@click.argument('group')
def add_user_to_group(username, group):
    """ Add an existing user to an existing group """
    _ldap_modify_user_and_group(username, group, ldap.MOD_ADD)


@num.command()
@click.argument('username')
@click.argument('group')
def remove_user_from_group(username, group):
    """ Remove an existing user from an existing group """
    _ldap_modify_user_and_group(username, group, ldap.MOD_DELETE)


def _ldap_connection():
    ldap_connection = ldap.initialize(config['ldap_server'])
    if (config['ldap_use_tls']):
        ldap_connection.start_tls_s()
    ldap_connection.simple_bind_s(config['ldap_bind_dn'], config['ldap_password'])

    return ldap_connection


def _ldap_remove_group(group, ou):
    # check if another group exist with same name
    if not group_exists(group):
        click.echo("Error: Group name %s does not exist." % group)
        sys.exit(1)

    click.echo("Info: Removing group %s in %s." % (group, ou))

    try:
        ldap_connection = _ldap_connection()
        results = ldap_connection.search_s(base=ou,
                                           scope=ldap.SCOPE_SUBTREE,
                                           filterstr='cn=' + group,
                                           attrlist=['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Error: Unable to remove group %s in %s." % (group, ou))
        click.echo(str(e))
        sys.exit(1)


def _ldap_modify_user_and_project(parent, username, project, ldap_mod):
    if not username_exists(username):
        click.echo("Error: Username %s does not exist." % username)
        sys.exit(1)

    if not project_group_exists(project, parent):
        click.echo("Error: Project group %s does not exist." % project)
        sys.exit(1)

    (project_group, ou) = get_project_group_and_ou(project, parent)

    return _ldap_modify_any_user_group(username, project_group, ou, ldap_mod)


def _ldap_modify_user_and_application(username, app, ldap_mod):
    if not username_exists(username):
        click.echo("Error: Username %s does not exist." % username)
        sys.exit(1)

    group_object_class = application_group_objectclass(app)
    if not group_object_class:
        click.echo("Error: Application group %s does not exist." % app)
        sys.exit(1)

    if group_object_class == "groupOfNames":
        ldap_connection = _ldap_connection()
        results = ldap_connection.search_s(base=config['ldap_base_dn'],
                                           scope=ldap.SCOPE_SUBTREE,
                                           filterstr='uid=' + username,
                                           attrlist=['dn'])
        uid_or_dn = results[0][0]
        ldap_connection.unbind_s()
    else:
        uid_or_dn = username

    (application_group, ou) = get_application_group_and_ou(app)

    return _ldap_modify_any_user_group(uid_or_dn, app, ou, ldap_mod, group_object_class)


def _ldap_modify_user_and_group(username, group, ldap_mod):
    if not username_exists(username):
        click.echo("Error: Username %s does not exist." % username)
        sys.exit(1)

    if not group_exists(group):
        click.echo("Error: Group name %s does not exist." % group)
        sys.exit(1)

    (group, ou) = get_group_and_ou(group)

    return _ldap_modify_any_user_group(username, group, ou, ldap_mod)


def _ldap_modify_any_user_group(uid_or_dn, group, ou, ldap_mod, group_object_class="posixGroup"):
    #  ldap_mod can be ldap.MOD_DEL or ldap.MOD_ADD

    group_dn = 'cn={group},{ou}'.format(group=group, ou=ou)
    _ldap_modify_any_group_by_dn(uid_or_dn, group_dn, ldap_mod, group_object_class)

    return group


def _ldap_modify_any_group_by_dn(uid_or_dn, group_dn, ldap_mod, group_object_class="posixGroup"):
    #  ldap_mod can be ldap.MOD_DEL or ldap.MOD_ADD

    try:
        # modify ldap group for user
        ldap_connection = _ldap_connection()
        if group_object_class == "groupOfNames":
            mod_attrs = [(ldap_mod, 'member', [uid_or_dn.encode()])]
        else:
            mod_attrs = [(ldap_mod, 'memberUid', [uid_or_dn.encode()])]
        ldap_connection.modify_s(group_dn, mod_attrs)
        ldap_connection.unbind_s()
        click.echo("Info: Performed operation with user entry %s on group entry %s objectClass=%s." % (uid_or_dn, group_dn, group_object_class))
    except ldap.LDAPError as e:
        click.echo("Error: Unable to perform operation with user entry %s on group entry %s objectClass=%s." % (uid_or_dn, group_dn, group_object_class))
        click.echo(str(e))
        sys.exit(1)


@num.command()
@click.option('-p', '--parent', default=None)
@click.option('--readwrite/--readonly', default=True)
@click.option('--scratch', default=None)
@click.argument('group')
@click.argument('project')
def add_group_to_project(parent, readwrite, group, project, scratch):
    """ Add an existing group to an existing project """

    mode = 'project' if not scratch else 'scratch'

    if not group_exists(group):
        click.echo("Error: Group %s does not exist." % group)
        sys.exit(1)

    if not project_group_exists(project, parent):
        click.echo("Error: Project group %s does not exist." % project)
        sys.exit(1)

    if readwrite:
        rights = 'rwx'
    else:
        rights = 'rx'

    project_path = get_project_path(project, parent, mode)

    acl_options = '-R'
    _set_acl(project_path, group, acl_options, remove=True)
    _set_acl(project_path, group, acl_options, rights=rights)


@num.command()
@click.option('--mode', type=click.Choice(['home', 'project', 'scratch']), default="project")
@click.argument('path')
@click.option('-sq', '--softquota', required=True)
@click.option('-iq', '--inodequota', default=None)
def set_quota(mode, path, softquota, inodequota):
    """ Set a quota to a project or home directory"""
    groupname = os.path.basename(path)
    gid_number = _get_group_gid(groupname)
    _set_quota(mode, path, gid_number, softquota, inodequota)


def _set_quota(mode, path, gid, softquota, inodequota=None):
    """ Set a quota to a project or home directory"""
    name = os.path.basename(path)
    matches = re.match(r'^(.*\d)\s*([a-zA-Z]*)$', softquota)
    size_str, unit_str = matches.groups()
    hardquota = str(round(int(size_str) * config['default_hard_quota_factor'])) + unit_str
    # todo: should manage the factor when quota is passed in T To G or Go ...
    # should work well for Go or G
    inodesoft = round(int(size_str) * config['default_inode_factor'])
    if inodequota:
        inodesoft = int(inodequota)

    inodehard = round(inodesoft * config['default_hard_quota_factor'])

    click.echo("Info: Setting up quota %s (%s) and inode limit %s (%s) for gid %s on %s." % (softquota, hardquota, inodesoft, inodehard, gid, path))

    # todo check if it work if there are not all var in string template
    cmd = config[mode + '_quota_template'].format(NAME=name,
                                                  PATH=path,
                                                  GIDNUMBER=gid,
                                                  SOFTQUOTA=softquota,
                                                  HARDQUOTA=hardquota,
                                                  INODESOFTLIMIT=inodesoft,
                                                  INODEHARDLIMIT=inodehard)

    run_command(config['client_server'], config['client_user'], cmd, hide=True)


@num.command()
@click.argument('path')
def set_acl(path):
    """ Set a acl to a project or home directory"""
    groupname = os.path.basename(path)

    acl_options = '-R'
    # todo should remove all acl (not only group one)
    # with setfacl -R -bn /shared/projects/<groupname>
    _set_acl(path, groupname, acl_options, remove=True)
    _set_acl(path, groupname, acl_options, rights='rwx')


def _set_acl(path, group, acl_options='', rights='', remove=False):
    """ Set a acl to a project or home directory"""
    groupname = os.path.basename(path)
    gid_number = _get_group_gid(groupname)

    mode = 'setfacl_project_template'
    if config.get('setfacl_right_uppercase', False):
        rights = rights.upper()

    if remove:
        mode = 'setfacl_project_remove_template'

    cmd = config[mode].format(PATH=path,
                              GROUP=group,
                              GIDNUMBER=gid_number,
                              OPTIONS=acl_options,
                              RIGHTS=rights)
    run_command(config['client_server'], config['client_user'], cmd, hide=True)


def run_command(server, user, command, hide=False, warn=False):
    client = Connection(server, user=user, connect_timeout=10)
    if server == 'localhost':
        res = client.local(command, hide=hide, warn=warn, pty=True)
    else:
        res = client.run(command, hide=hide, warn=warn, pty=True, in_stream=False)
    client.close()
    return res


def get_group_and_ou(name):
    ou = config['ldap_groups_ou']
    group = name

    return (group, ou)


def get_project_group_and_ou(name, parent):
    if parent:
        ou = 'ou=%s,%s' % (parent, config['ldap_projects_groups_ou'])
        project_group = "%s_%s" % (parent, name)
    else:
        ou = config['ldap_projects_groups_ou']
        project_group = name

    return (project_group, ou)


def get_application_group_and_ou(name):
    ou = config['ldap_applications_groups_ou']
    app = name

    return (app, ou)


def get_project_path(name, parent=None, mode="project"):
    if (mode + '_home') in config.keys():
        home = config[mode + '_home']
        if parent:
            project_path = os.path.join(home, parent, name)
        else:
            project_path = os.path.join(home, name)
        return project_path
    else:
        return ""


def project_group_exists(name, parent=None):
    """ Check if a project group with the given name already exists """
    ldap_connection = _ldap_connection()

    (project_group, ou) = get_project_group_and_ou(name, parent)

    res = ldap_connection.search_s(base=ou,
                                   scope=ldap.SCOPE_SUBTREE,
                                   filterstr='(&(objectClass=posixGroup)(cn=%s))' % project_group)
    ldap_connection.unbind_s()
    return len(res) > 0


def application_group_objectclass(name):
    """ Return the objectClass of an app group if it exists """
    ldap_connection = _ldap_connection()

    (app_group, ou) = get_application_group_and_ou(name)

    res = ldap_connection.search_s(base=ou,
                                   scope=ldap.SCOPE_SUBTREE,
                                   filterstr='(&(|(objectClass=posixGroup)(objectClass=groupOfNames))(cn=%s))' % app_group,
                                   attrlist=['objectClass'])
    ldap_connection.unbind_s()
    if len(res) > 0:
        return res[0][1].get('objectClass')[0].decode('utf-8')


def retrieve_subprojects(name):
    subprojects = []
    ldap_connection = _ldap_connection()
    base = "ou=%s,%s" % (name, config['ldap_projects_groups_ou'])
    results = ldap_connection.search_s(base=base,
                                       scope=ldap.SCOPE_ONELEVEL,
                                       filterstr='objectClass=posixGroup',
                                       attrlist=['cn'])
    ldap_connection.unbind_s()
    for result in results:
        subproject_group = result[1].get('cn')[0].decode('utf-8')
        subproject = subproject_group[len(name) + 1:]
        subprojects.append(subproject)

    return subprojects


def _ldap_add_group(dn, gid_number=None, group_object_class="posixGroup"):
    _ldap_add_ou_recursively(dn)
    (attr, val) = dn.split(',')[0].split('=')
    if attr == 'cn':
        if not group_exists(val):
            modlist = {
                "objectClass": [group_object_class.encode(), "top".encode()],
            }
            if group_object_class == "posixGroup":
                modlist["gidNumber"] = [(str(gid_number)).encode()]
            try:
                ldap_connection = _ldap_connection()
                ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
                ldap_connection.unbind_s()
                click.echo("Info: Added group entry %s with attribute objectClass=%s." % (dn, group_object_class))
            except ldap.LDAPError as e:
                click.echo("Error: Unable to create group entry %s. with attribute objectClass=%s." % (dn, group_object_class))
                click.echo(str(e))
                sys.exit(1)


def _ldap_add_ou_recursively(dn):
    spl_dn = dn.split(',')
    len_spl = len(spl_dn)
    len_cpt = len_spl
    # we should start from the end of the dn
    while len_cpt >= 0:
        len_cpt -= 1
        (attr, val) = spl_dn[len_cpt].split('=')
        if attr == 'ou':
            ou_to_add = ','.join(spl_dn[len_cpt:len_spl])
            _ldap_add_ou(ou_to_add)


def _ldap_add_ou(dn):
    (attr, val) = dn.split(',')[0].split('=')
    ldap_connection = _ldap_connection()
    if attr == 'ou':
        # check if ou already exist
        try:
            res = ldap_connection.search_s(base=dn,
                                           scope=ldap.SCOPE_BASE)
        except ldap.LDAPError:
            res = []

        if len(res) == 0:
            # add ou if no res
            modlist = {
                "objectClass": ["organizationalUnit".encode(), "top".encode()],
                "ou": [val.encode()]
            }
            try:
                ldap_connection.add_s(dn, ldap.modlist.addModlist(modlist))
                click.echo("Info: Added ou entry %s." % str(val))
            except ldap.LDAPError as e:
                click.echo("Error: Unable to add ou entry %s." % str(val))
                click.echo(str(e))
                sys.exit(1)
    else:
        click.echo("Info: %s is not an ou." % dn)

    ldap_connection.unbind_s()


def project_ou_exists(name):
    ldap_connection = _ldap_connection()
    res = ldap_connection.search_s(base=config['ldap_projects_groups_ou'],
                                   scope=ldap.SCOPE_ONELEVEL,
                                   filterstr='(&(objectClass=organizationalUnit)(ou=%s))' % name)
    ldap_connection.unbind_s()
    return len(res) > 0


def remove_project_ou(name):

    try:
        ldap_connection = _ldap_connection()
        results = ldap_connection.search_s(base=config['ldap_projects_groups_ou'],
                                           scope=ldap.SCOPE_ONELEVEL,
                                           filterstr='(&(objectClass=organizationalUnit)(ou=%s))' % name,
                                           attrlist=['dn'])
        for dn, entry in results:
            ldap_connection.delete_s(dn)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Error: Unable to delete project ou %s." % name)
        click.echo(str(e))
        sys.exit(1)


def _get_gid(group_dn):
    res = []
    try:
        ldap_connection = _ldap_connection()
        res = ldap_connection.search_s(base=group_dn,
                                       scope=ldap.SCOPE_BASE)
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo("Error: Unable to find gid for dn %s." % group_dn)
        click.echo(str(e))
        sys.exit(1)

    if len(res) == 1:
        return res[1]['gidNumber'].decode()

    click.echo("Error: Unable to find gid for dn %s." % group_dn)
    sys.exit(1)


def _get_ldap_group(name_or_gid):
    res = []
    try:
        ldap_connection = _ldap_connection()
        res = ldap_connection.search_s(base=config['ldap_base_dn'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='(&(|(objectClass=posixGroup)(objectClass=groupOfNames))(|(cn=%s)(gidNumber=%s)))' % (name_or_gid, name_or_gid))
        ldap_connection.unbind_s()
    except ldap.LDAPError as e:
        click.echo(str(e))
        res = []

    return res


def _get_group_gid(name):
    ldap_group = _get_ldap_group(name)

    if len(ldap_group) > 0:
        dn, entry = ldap_group[0]  # todo check if only one result
        gid_number = entry['gidNumber'][0].decode()
    else:
        click.echo("Error: Group name %s does not exist." % name)
        sys.exit(1)
    return gid_number


def _get_group_name(gid):
    ldap_group = _get_ldap_group(gid)

    if len(ldap_group) > 0:
        dn, entry = ldap_group[0]  # todo check if only one result
        name = entry['cn'][0].decode()
    else:
        click.echo("Error: Group id %s does not exist." % gid)
        sys.exit(1)
    return name


def group_exists(name_or_gid):
    """ Check if a project group with the given name or gid already exists """
    res = _get_ldap_group(name_or_gid)
    return len(res) > 0


def remove_project_group(name, parent=None):
    group = name if not parent else "%s_%s" % (parent, name)
    ou = config['ldap_projects_groups_ou'] if not parent else "ou=%s,%s" % (parent, config['ldap_projects_groups_ou'])

    _ldap_remove_group(group, ou)


def remove_slurm_account(name):
    _slurm_clean_default_account(name)
    click.echo("Info: Removing Slurm account %s." % name)
    account_cmd = 'sacctmgr -Q -i delete account %s' % name
    result = run_command(config['client_server'], config['client_user'], account_cmd, hide=True, warn=True)
    if not result.ok and "Nothing deleted" not in result.stdout:
        click.echo("Info: Failed to delete Slurm account %s." % name)


def remove_project_folder(name, parent=None, mode='project'):
    if (mode + '_home') in config.keys():
        home = config[mode + '_home']

        if parent:
            project_dir = os.path.join(home, parent, name)
        else:
            project_dir = os.path.join(home, name)

        if project_dir and len(project_dir) > len(home):
            click.echo("Info: Removing project directory %s." % project_dir)
            rm_cmd = 'rm -rf %s' % project_dir
            run_command(config['client_server'], config['client_user'], rm_cmd, hide=True)


def get_next_gid_number(min_gid_number=config['min_gid_number']):
    """ get the next free gid in LDAP """
    ldap_connection = _ldap_connection()
    results = ldap_connection.search_s(base=config['ldap_groups_ou'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='objectClass=posixGroup',
                                       attrlist=['gidNumber'])
    ldap_connection.unbind_s()

    if len(results) == 0:
        return min_gid_number
    else:
        gids = sorted([int(result[1].get('gidNumber')[0]) for result in results])
        return min(set(range(min_gid_number, gids[-1] + 2)).difference(gids))


def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


def generate_username(firstname, lastname, cpt_firstname=1, cpt_lastname=0):
    """ generate username first letter of firstname and lastname """
    lastname2 = strip_accents(lastname).lower().replace(" ", "").replace("'", "")
    firstname2 = strip_accents(firstname).lower().replace(" ", "").replace("-", "").replace("'", "")
    username = firstname2[:cpt_firstname] + lastname2[0:len(lastname2) - cpt_lastname]  # we need explicit len as 0:-0 return empty string

    if username_exists(username) and len(firstname2) >= cpt_firstname:
        username = generate_username(firstname2, lastname2, cpt_firstname + 1, cpt_lastname)

    if username_exists(username) and len(lastname2) > cpt_lastname + 1:
        username = generate_username(firstname2, lastname2, 1, cpt_lastname + 1)

    if username_exists(username):
        username = generate_username(firstname2, lastname2 + str(random.randint(1, 42)))

    return username


def username_exists(username):
    """ check if username already exists """
    ldap_connection = _ldap_connection()
    results = ldap_connection.search_s(base=config['ldap_base_dn'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='uid=' + username,
                                       attrlist=['uidNumber'])
    ldap_connection.unbind_s()
    return len(results) > 0


def email_exists(email):
    """ check if email already exists """
    ldap_connection = _ldap_connection()
    results = ldap_connection.search_s(base=config['ldap_base_dn'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='mail=' + email,
                                       attrlist=['uidNumber'])
    ldap_connection.unbind_s()
    return len(results) > 0


def get_user(username):
    """ retrieve user's firstname from ldap """
    ldap_connection = _ldap_connection()
    results = ldap_connection.search_s(base=config['ldap_base_dn'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='uid=' + username,
                                       attrlist=['givenName', 'sn', 'mail'])
    ldap_connection.unbind_s()
    if len(results) > 0:
        return {'firstname': results[0][1].get('givenName')[0].decode('utf-8'),
                'lastname': results[0][1].get('sn')[0].decode('utf-8'),
                'email': results[0][1].get('mail')[0].decode('utf-8')}
    else:
        return None


def get_next_uid_number(min_uid_number=config['min_uid_number']):
    """ get the next free uid in LDAP """
    ldap_connection = _ldap_connection()
    results = ldap_connection.search_s(base=config['ldap_people_ou'],
                                       scope=ldap.SCOPE_SUBTREE,
                                       filterstr='objectClass=posixAccount',
                                       attrlist=['uidNumber'])
    ldap_connection.unbind_s()

    if len(results) == 0:
        return min_uid_number
    else:
        uids = sorted([int(result[1].get('uidNumber')[0]) for result in results])
        return min(set(range(min_uid_number, uids[-1] + 2)).difference(uids))


def get_next_uid_gid_number(min_uid_number=config['min_uid_number'], min_gid_number=config['min_gid_number']):
    """ get the next free common uid/gid in LDAP """
    ldap_connection = _ldap_connection()
    results_uid = ldap_connection.search_s(base=config['ldap_people_ou'],
                                           scope=ldap.SCOPE_SUBTREE,
                                           filterstr='objectClass=posixAccount',
                                           attrlist=['uidNumber'])
    results_gid = ldap_connection.search_s(base=config['ldap_groups_ou'],
                                           scope=ldap.SCOPE_SUBTREE,
                                           filterstr='objectClass=posixGroup',
                                           attrlist=['gidNumber'])
    ldap_connection.unbind_s()

    if len(results_uid) == 0 and len(results_gid) == 0:
        return max(min_uid_number, min_gid_number)
    else:
        uids = sorted([int(result[1].get('uidNumber')[0]) for result in results_uid])
        gids = sorted([int(result[1].get('gidNumber')[0]) for result in results_gid])
        return min(set([*range(min_uid_number, uids[-1] + 2), *range(min_gid_number, gids[-1] + 2)]).difference(uids + gids))


def _run_extra_cmd(directory_type, path):
    """
    Run an optional extra script in the home, project or scratch directory
    type in ['home', 'project', 'scratch']
    """
    if config.get(directory_type + '_extra_cmd_template', False):
        cmd = config[directory_type + '_extra_cmd_template'].format(PATH=path)
        run_command(config['client_server'], config['client_user'], cmd, hide=True)


def send_email_create_user(email, firstname, username, password):
    template = jinja_env.get_template('create_user.j2')
    envelope = Envelope(
        from_addr=(config['notification_sender'], config['notification_sender_name']),
        to_addr=(email),
        subject=config['create_user_subject'],
        text_body=template.render(firstname=firstname,
                                  username=username,
                                  password=password)
    )

    # Send the envelope using an ad-hoc connection...
    envelope.send(config['smtp_host'],
                  login=config.get('smtp_login', None),
                  password=config.get('smtp_password', None),
                  tls=config.get('smtp_usetls', False))


def send_email_add_user_to_project(username, project_group, project, parent, slurm):
    user = get_user(username)
    template = jinja_env.get_template('add_user_to_project.j2')
    envelope = Envelope(
        from_addr=(config['notification_sender'], config['notification_sender_name']),
        to_addr=(user['email']),
        subject=config['add_user_to_project_subject'].format(project_group),
        text_body=template.render(firstname=user['firstname'],
                                  project=project,
                                  project_path=get_project_path(project, parent),
                                  scratch_path=get_project_path(project, parent, "scratch"),
                                  project_group=project_group,
                                  slurm=slurm)
    )

    # Send the envelope using an ad-hoc connection...
    envelope.send(config['smtp_host'],
                  login=config.get('smtp_login', None),
                  password=config.get('smtp_password', None),
                  tls=config.get('smtp_usetls', False))


def generate_password_hash(password):
    return '{CRYPT}' + sha512_crypt.hash(password)
