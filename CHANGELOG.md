
<a name="0.6.4"></a>
## [0.6.4](/compare/0.6.3...0.6.4) (2021-11-23)

### Remove

* remove useless sort
* remove user from project partition too

### Try

* try to remove user from all partition related to project


<a name="0.6.3"></a>
## [0.6.3](/compare/0.6.2...0.6.3) (2021-11-23)

### Ensure

* ensure mins is an str before concat

### Fix

* fix for pylama

### Fix

* Fix gitlab ci tags for docker runner

### Set

* set max mins on partition for demo

### Update

* update gitlab ci to force docker runner + update image

### Merge Requests

* Merge branch 'set_max_minutes_for_demo_account' into 'master'


<a name="0.6.2"></a>
## [0.6.2](/compare/0.6.1...0.6.2) (2021-11-22)

### Try

* try to add user on fast partition on user set default partition + add some log

### Use

* use demo as default account for all user

### Merge Requests

* Merge branch 'use_demo_as_default_account_for_all' into 'master'


<a name="0.6.1"></a>
## [0.6.1](/compare/0.5.14...0.6.1) (2021-10-27)

### Add

* add group create and remove command
* add add-user-to-group and remove-user-from group command

### Clean

* clean code after conflict

### Fix

* fix variable name
* fix variable name and method call
* fix for pylama

### Move

* move code

### Merge Requests

* Merge branch 'add_group_mngn_without_project' into 'master'


<a name="0.5.14"></a>
## [0.5.14](/compare/0.5.13...0.5.14) (2021-09-17)

### Clean

* clean also .gitlab ci file

### Configure

* Configure SAST in `.gitlab-ci.yml`, creating this file if it does not already exist

### Disable

* disable before script for sast

### Do

* do test before build

### Remove

* remove aliase method as they have been moved in mel

### Merge Requests

* Merge branch 'clean_alias_command' into 'master'
* Merge branch 'set-sast-config-2' into 'master'


<a name="0.5.13"></a>
## [0.5.13](/compare/0.5.12...0.5.13) (2021-09-02)

### Fix

* fix service account creation


<a name="0.5.12"></a>
## [0.5.12](/compare/0.5.11...0.5.12) (2021-06-24)

### Add

* add a smart comment
* add set-acl command

### Remove

* remove not existing arg in set-acl-command


<a name="0.5.11"></a>
## [0.5.11](/compare/0.5.10...0.5.11) (2021-06-24)

### Add

* add gidnumber as var for acl template


<a name="0.5.10"></a>
## [0.5.10](/compare/0.5.9...0.5.10) (2021-06-21)

### Add

* add a try catch on ou search

### Fix

* fix for pylama


<a name="0.5.9"></a>
## [0.5.9](/compare/0.5.8...0.5.9) (2021-06-21)

### Auto

* auto add main ou if does not exist

### Do

* do not failed on user group remove (as it may not exist)

### Fix

* fix split to find ou


<a name="0.5.8"></a>
## [0.5.8](/compare/0.5.7...0.5.8) (2021-06-18)

### Add

* add default gid number management
* add config var create_user_group


<a name="0.5.7"></a>
## [0.5.7](/compare/0.5.6...0.5.7) (2021-06-17)

### Allow

* allow service account without email

### Fix

* fix for pylama


<a name="0.5.6"></a>
## [0.5.6](/compare/0.5.5...0.5.6) (2021-06-11)

### Add

* add group aliases command
* add a show ldap command in num

### Fix

* fix for pylama

### Merge Requests

* Merge branch 'add_group_aliases' into 'master'


<a name="0.5.5"></a>
## [0.5.5](/compare/0.5.4...0.5.5) (2021-06-10)

### Add

* Add a switch add_user_to_group_cn

### Fix

* Fix the switch quota_home

### Reorganisation

* reorganisation of the config file

### Merge Requests

* Merge branch 'allow_different_client_server' into 'master'


<a name="0.5.4"></a>
## [0.5.4](/compare/0.5.3...0.5.4) (2021-06-10)

### Add

* Add postfix aliases

### Merge Requests

* Merge branch 'add_postfix_aliases' into 'master'


<a name="0.5.3"></a>
## [0.5.3](/compare/0.5.2...0.5.3) (2021-06-08)

### Add

* Add switch ACL right in uppercase
* Add switch quota_home (for ABiMS usecase)

### Allow

* Allow name in set_quota function to be compliant with fuildfs

### Guess

* Guess in set_quota the directory name

### Set

* set name var for quota as optionnal var

### Merge Requests

* Merge branch 'switch_home_quota' into 'master'
* Merge branch 'switch_acl_right_upper' into 'master'
* Merge branch 'quota_name_template' into 'master'


<a name="0.5.2"></a>
## [0.5.2](/compare/0.5.1...0.5.2) (2021-04-01)

### Add

* add an option to set inode quota

### Ensure

* ensure to use int for *

### Merge Requests

* Merge branch 'add_an_option_to_set_inode_quota' into 'master'


<a name="0.5.1"></a>
## [0.5.1](/compare/0.5.0...0.5.1) (2021-03-08)

### Add

* add missing ou param
* add remove user from partition method
* add a method to add a user to a partition

### Clean

* clean code organisation

### Fix

* fix remove from part

### Remove

* remove return from modify method

### Merge Requests

* Merge branch 'add_user_to_part' into 'master'


<a name="0.5.0"></a>
## [0.5.0](/compare/0.4.10...0.5.0) (2021-03-02)

### Add

* add a try/cache on user project modification to show explicit error

### Enhance

* enhance the conf.yml.sample: add fluidfs template for quota

### Merge Requests

* Merge branch 'enhance_conf' into 'master'


<a name="0.4.10"></a>
## [0.4.10](/compare/0.4.9...0.4.10) (2021-02-16)

### Update

* update dep version


<a name="0.4.9"></a>
## [0.4.9](/compare/0.4.8...0.4.9) (2021-01-20)

### Fix

* fix syntax + fix for pylama
* fix mail template on user creation


<a name="0.4.8"></a>
## [0.4.8](/compare/0.4.7...0.4.8) (2020-12-18)

### Fix

* fix again gid number
* fix num get gid for set quota


<a name="0.4.7"></a>
## [0.4.7](/compare/0.4.6...0.4.7) (2020-12-18)

### Add

* add a check if group exist on set quota

### Love

* love pylama


<a name="0.4.6"></a>
## [0.4.6](/compare/0.4.5...0.4.6) (2020-12-18)

### Add

* add a command to set quota

### Fix

* fix set-quota command param
* fix for pylama


<a name="0.4.5"></a>
## [0.4.5](/compare/0.4.4...0.4.5) (2020-12-18)

### Add

* add inode conf sample
* add inode limite + remove hardquota option

### Fix

* fix variable name in example conf


<a name="0.4.4"></a>
## [0.4.4](/compare/0.4.3...0.4.4) (2020-12-17)

### Add

* add a smart comment
* add gid_number var in quota template

### Fix

* fix for pylama

### Merge Requests

* Merge branch 'add_gid_number_var' into 'master'


<a name="0.4.3"></a>
## [0.4.3](/compare/0.4.2...0.4.3) (2020-12-15)

### Fix

* fix remove parent acl on sub project creation
* fix variable name on remove project


<a name="0.4.2"></a>
## [0.4.2](/compare/0.4.1...0.4.2) (2020-12-14)

### Hide

* hide quota command as it break script which use num


<a name="0.4.1"></a>
## [0.4.1](/compare/0.4.0...0.4.1) (2020-12-10)

### Add

* add basic check before rm

### Fix

* fix os path syntax
* fix home dir variable name

### Use

* use os.path.join for path join


<a name="0.4.0"></a>
## [0.4.0](/compare/0.3.3...0.4.0) (2020-12-01)

### Acl

* acl configurable

### Add

* add a sufix on email config var

### Change

* change in cmd template

### Def

* def send_email_*()
* def _set_acl() ; rename a config var

### Emails

* emails configurable

### Get_registration_data

* get_registration_data -> DEPRECATED

### Homedir

* homedir configurable

### Quota

* quota factorization + configurable - v2
* quota factorization + configurable

### Remove

* remove def get_registration_data

### Rename

* rename setacl variable because they are used for project
* rename users_home var to user_home (because for project_home we dont have a s)

### Merge Requests

* Merge branch 'abims' into 'master'


<a name="0.3.3"></a>
## [0.3.3](/compare/0.3.2...0.3.3) (2020-05-20)

### Add

* add .eggs in gitignore
* add dynamic hard quota (based on softquota)

### Love

* love pylama

### Update

* update conf exemple with hard quota factor

### Merge Requests

* Merge branch 'auto_hard_quota' into 'master'


<a name="0.3.2"></a>
## [0.3.2](/compare/0.3.1...0.3.2) (2020-03-25)

### Add

* add a command to set the last project added as default account for user

### Merge Requests

* Merge branch 'add_user_default_account' into 'master'


<a name="0.3.1"></a>
## [0.3.1](/compare/0.3.0...0.3.1) (2020-03-11)

### Find

* find max uid or gid to set the same value for both

### Merge Requests

* Merge branch 'use_same_uid_and_gi_for_user' into 'master'


<a name="0.3.0"></a>
## [0.3.0](/compare/0.2.4...0.3.0) (2020-03-11)

### Try

* try to fix gid conflict (between user group and project)

### Merge Requests

* Merge branch 'fix_gid_issue' into 'master'


<a name="0.2.4"></a>
## [0.2.4](/compare/0.2.3...0.2.4) (2019-12-12)

### Remove

* remove next gid limit

### Merge Requests

* Merge branch 'remove_gid_limit' into 'master'


<a name="0.2.3"></a>
## [0.2.3](/compare/0.2.2...0.2.3) (2019-12-09)

### Release

* Release 0.2.3
* Release 0.2.3
* Release 0.2.3

### Set

* set generated password size to 10

### Merge Requests

* Merge branch 'change_password_size' into 'master'


<a name="0.2.2"></a>
## [0.2.2](/compare/0.2.1...0.2.2) (2019-10-11)

### Add

* add -y in apt command for gitlab ci
* add an a update to install dependency in gitlab ci
* add a build step in gitlab.ci

### Install

* install deps for gitlab.ci build

### Release

* Release 0.2.1

### Release

* release only on tags

### Try

* try (last) again (again) to upload release on tag change
* try again (again) to upload release on tag change
* try again to upload release on tag change
* try to clean gitlab ci
* try again with gitlab release in giltab ci
* try with explicit file name for gitlab-ci release
* try to explicit use of private token in gilabci (for release)
* try with gitlab-release


<a name="0.2.1"></a>
## [0.2.1](/compare/0.2.0...0.2.1) (2019-10-11)

### Add

* add explicit encoding string
* add some blanck line for pylama
* add --version option to num
* add 2 blank as pylama ask for it

### Release

* Release 0.2.1

### Remove

* remove slurm comment and args from remove_user_from_project

### Try

* Try to explicit set mail string as unicode (as they contain accent)

### Try

* try to add a remove user from project feature

### Use

* use the existing ldap flag
* use scm to find current version

### Merge Requests

* Merge branch 'unicode_string' into 'master'
* Merge branch 'add_option_to_remove_user_from_project' into 'master'
* Merge branch 'show_version' into 'master'


<a name="0.2.0"></a>
## 0.2.0 (2019-10-03)

### First

* First Public Release

