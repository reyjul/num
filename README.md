# NNCR cluster User Manager

## Install for developer

Clone the repository

```
git clone repository_url.git
```

`cd` to the repository folder

Create a python virtual environment for the project

```
python3 -m venv env
```

Activate the python virtual environment

```
source env/bin/activate
```

Install the package in development mode

```
pip install -e .
```

## Usage

num can be used locally or remotely over ssh. Following variables must be set accordingly:

```
client_server: localhost
client_user: root
```

If using num over ssh, you must first copy your public key to the remote machine:

```
ssh-copy-id root@slurm-client-dev.dev.ifb.local
ssh-copy-id root@slurm-client.ifb.local
```

### Help
```
num --help
```

### Run on test ldap
```
num --test <command>
```

### Create a user in ldap
Create a user account and the associated user-group in the ldap directory.
```
num <--test> create-user --firstname=<prenom> --lastname=<nom> --password=<motdepasse> --email=<email>
```

### Create a project in ldap and on the shared storage
Create a project: ldap group <project_name> in OU "ou=projects,ou=groups,dc=ifb,dc=local", account <project_name> in slurm, folder /shared/projects/<project_name> on the storage with acls
```
num <--test> create-project <project_name>
```

### Add a user to a project
Add a user to the ldap memberUid of the project <project_name> and add user to the slurm acccount associated to this project
```
num <--test> add-user-to-project <username> <project_name> 
```

### Remove a user from ldap
Delete a user from ldap, his associated user-group, and remove user from all groups.
```
num <--test> remove-user <username>
```
