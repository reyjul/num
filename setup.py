import os

from setuptools import setup

LONG_DESC = open(os.path.join(os.path.dirname(__file__), "README.md")).read()

setup(
    name="num",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description="NNCR cluster User Manager",
    long_description=LONG_DESC,
    author="NNCR Taskforce",
    author_email="gt-nncr-cluster@groupes.france-bioinformatique.fr",
    license="GNU GENERAL PUBLIC LICENSE v2",
    url="",
    zip_safe=False,
    install_requires=[
        'click==8.0',
        'Envelopes==0.4',
        'fabric==2.6',
        'grampg==0.3.0',
        'jinja2==2.11',
        'passlib==1.7',
        'python-ldap==3.4',
        'PyYAML==6.0',
        'requests==2.27',
        'markupsafe==2.0.1'
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        'Natural Language :: English',
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
    py_modules=['num'],
    entry_points={
        'console_scripts': [
            'num = num:num'
        ]
    },
    data_files=[
        ('config', ['conf.yml']),
        ('templates', ['templates/add_user_to_project.j2', 'templates/create_user.j2']),
    ]
)
